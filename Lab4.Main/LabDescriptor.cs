﻿using System;
using ComponentFramework;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Lab4.Component1.Class1);
        public static Type Component2 = typeof(Lab4.Component2.Class2);

        public static Type RequiredInterface = typeof(Lab4.Contract.Interface1);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => component2;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { (container as Container).RegisterComponent((IComponent)component); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => (container as Container).DependenciesResolved;

        #endregion

        #region P3

        public static Type Component2B = typeof(Lab4.Component2B.Class3);

        #endregion
    }
}
