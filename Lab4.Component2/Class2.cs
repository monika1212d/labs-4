﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Class2 : AbstractComponent, Lab4.Contract.Interface1
    {
        public Class2()
        {
            RegisterProvidedInterface<Lab4.Contract.Interface1>(this);
        }

        public void Metoda1()
        {
            Console.WriteLine("Pierwsza metoda z komponentu 2");
        }

        public void Metoda2()
        {
            Console.WriteLine("Druga metoda z komponentu 2");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
