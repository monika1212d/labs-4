﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;



namespace Lab4.Component1
{
    public class Class1 : AbstractComponent, Lab4.Contract.Interface1
    {
        Lab4.Contract.Interface1 interfejs = null;

        public Class1()
        {
            RegisterRequiredInterface<Lab4.Contract.Interface1>();
        }

        public Class1(Lab4.Contract.Interface1 interfejs)
        {
            this.interfejs = interfejs;
            RegisterProvidedInterface<Lab4.Contract.Interface1>(this.interfejs);
        }

        public void Metoda1()
        {
            interfejs.Metoda1();
        }

        public void Metoda2()
        {
            interfejs.Metoda2();
        }

        public override void InjectInterface(Type type, object impl)
        {
            
        }
    }
}
